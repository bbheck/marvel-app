//
//  SearchItemTableViewCell.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/2/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class SearchItemTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var cardImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    
    weak var character: Character?

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func loadContent() {
        guard let _character = character else {
            return
        }
        
        cardImageView.setImageWithString(_character.standardMediumUrlString)
        nameLabel.text = _character.name
    }
    
}
