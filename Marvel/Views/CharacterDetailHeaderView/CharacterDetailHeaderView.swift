//
//  CharacterDetailHeaderView.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/1/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class CharacterDetailHeaderView: UIView {

    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var backgroundImageViewTopConstraint: NSLayoutConstraint!
    
    var imageStringUrl: String = "" {
        didSet {
            backgroundImageView.setImageWithString(imageStringUrl)
        }
    }
    
    @available(*, unavailable)
    init() {
        super.init(frame: CGRectZero)
    }
    
    @available(*, unavailable)
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func createWithSize(size: CGSize, stringUrl: String) -> CharacterDetailHeaderView {
        let headerView = CharacterDetailHeaderView.fromNib() as CharacterDetailHeaderView
        headerView.frame.size = size
        headerView.imageStringUrl = stringUrl
        
        return headerView
    }
    
    func setContentOffset(offset: CGPoint) {
        if offset.y <= frame.size.height {
            backgroundImageViewTopConstraint.constant = offset.y
        } else {
            backgroundImageViewTopConstraint.constant = frame.size.height
        }
    }

}
