//
//  CardCollectionViewCell.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/30/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    weak var card: Card?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.addShadowBorder()
        imageView.backgroundColor = Config.backgroundColor
    }
    
    func loadContent() {
        guard let _card = card else {
            return
        }
        
        imageView.setImageWithString(_card.portraintImageUrlString)
        titleLabel.text = _card.name
    }

}
