//
//  LinkTableViewCell.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/1/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class LinkTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    
    var titleText: String? {
        didSet {
            titleLabel.text = titleText
        }
    }
    
    class func openWithUrlString(string: String?) {
        guard let _string = string, url = NSURL(string: _string) else {
            return
        }
        
        UIApplication.sharedApplication().openURL(url)
    }
    
}
