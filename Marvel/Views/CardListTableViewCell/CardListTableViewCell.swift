//
//  CardListTableViewCell.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/1/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class CardListTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    var keyPath: String?
    var lastNumItems: Int = 0
    var items = [Card]() {
        didSet {
            collectionView.reloadData()
            if items.count > 0 {
                activityIndicator.stopAnimating()
            } else {
                activityIndicator.startAnimating()
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.registerCollectionViewCell(CardCollectionViewCell)
        collectionView.scrollsToTop = false
    }
    
}

// MARK: - UICollectionViewDataSource

extension CardListTableViewCell: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CardCollectionViewCell.reusableIdentifier, forIndexPath: indexPath) as! CardCollectionViewCell
        
        cell.card = items[indexPath.row]
        
        return cell
    }
    
}

// MARK: - UICollectionViewDelegate

extension CardListTableViewCell: UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        guard let destination = topViewController()?.storyboard?.getViewController(CardListViewController) else {
            return
        }
        destination.items = items
        destination.selectedRow = indexPath.row
        
        topViewController()?.presentViewController(destination, animated: true, completion: nil)
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        (cell as? CardCollectionViewCell)?.loadContent()
    }
    
}
