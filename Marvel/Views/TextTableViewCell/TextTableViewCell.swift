//
//  TextTableViewCell.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/1/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class TextTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    
    var titleText: String? {
        didSet {
            titleLabel.text = titleText?.isEmpty == true ? "Not available." : titleText
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
