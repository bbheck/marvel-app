//
//  CharacterListTableViewCell.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/29/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class CharacterListTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var nameBackgroudImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    
    static var ratio: CGFloat {
        return 0.4375
    }
    
    weak var character: Character?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameBackgroudImageView.addShadowBorder()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func loadContent() {
        guard let _character = character else {
            return
        }

        nameLabel.text = _character.name
        backgroundImageView.setImageWithString(_character.fullImageUrlString)
    }
    
}
