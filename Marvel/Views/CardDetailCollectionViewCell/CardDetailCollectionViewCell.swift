//
//  CardDetailCollectionViewCell.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/2/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class CardDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var cardImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var pageLabel: UILabel!
    
    weak var card: Card?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func loadContent(index index: Int, total: Int) {
        guard let _card = card else {
            return
        }
        
        cardImageView.setImageWithString(_card.fullImageUrlString)
        titleLabel.text = _card.name
        pageLabel.text = "\(index + 1)/\(total)"
    }

}
