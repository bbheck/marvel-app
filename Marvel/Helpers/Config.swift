//
//  Config.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/30/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

enum Config {
    static let navigationBarColor = UIColor(red: 12/255, green: 14/255, blue: 15/255, alpha: 1.0)
    static let backgroundColor = UIColor(red: 40/255, green: 44/255, blue: 48/255, alpha: 1.0)
    static let redColor = UIColor(red: 250/255, green: 35/255, blue: 44/255, alpha: 1.0)
}

enum Segue {
    static let toCharacterDetail = "toCharacterDetailSegue"
    static let toCardListModal = "toCardListModalSegue"
}