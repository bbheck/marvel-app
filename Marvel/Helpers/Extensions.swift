//
//  Extensions.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/30/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

import SDWebImage

// MARK: - UIStoryboard

extension UIStoryboard {
    
    func getViewController<T: UIViewController>(viewController: T.Type) -> T? {
        return instantiateViewControllerWithIdentifier(String(viewController)) as? T
    }
    
}

// MARK: - UIViewController

extension UIViewController {
    
    func setStatusBarBackgroundColor(color: UIColor) {
        guard let statusBar = UIApplication.sharedApplication().valueForKey("statusBarWindow")?.valueForKey("statusBar") as? UIView else {
            return
        }
        
        statusBar.backgroundColor = color
    }
    
}

// MARK: - UIView

extension UIView {
    
    class func fromNib<T : UIView>(nibNameOrNil: String? = nil) -> T {
        let v: T? = fromNib(nibNameOrNil)
        return v!
    }
    
    class func fromNib<T : UIView>(nibNameOrNil: String? = nil) -> T? {
        var view: T?
        let name: String
        if let nibName = nibNameOrNil {
            name = nibName
        } else {
            // Most nibs are demangled by practice, if not, just declare string explicitly
            name = "\(T.self)".componentsSeparatedByString(".").last!
        }
        let nibViews = NSBundle.mainBundle().loadNibNamed(name, owner: nil, options: nil)
        for v in nibViews {
            if let tog = v as? T {
                view = tog
            }
        }
        return view
    }
    
    func addShadowBorder(opacity opacity: Float = 0.5) {
        layer.shadowRadius = 3.0
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        layer.shadowOpacity = opacity
        layer.masksToBounds = false
    }
    
    func addBlurWithStyle(style: UIBlurEffectStyle) {
        let blurView = UIVisualEffectView(
            effect: UIBlurEffect(
                style: style
            )
        )
        blurView.frame = bounds
        blurView.autoresizingMask = [
            .FlexibleTopMargin, .FlexibleRightMargin,
            .FlexibleBottomMargin, .FlexibleLeftMargin,
            .FlexibleHeight, .FlexibleWidth
        ]
        
        addSubview(blurView)
    }
    
}

// MARK: - UITableView

extension UITableView {
    
    func registerTableViewCell(cell: ReusableCellIdentifiable.Type) {
        registerNib(
            UINib(
                nibName: String(cell),
                bundle: nil
            ),
            forCellReuseIdentifier: cell.reusableIdentifier
        )
    }
    
    func removeFooterViewWithAnimation(duration duration: NSTimeInterval = 0.6) {
        UIView.animateWithDuration(
            duration,
            delay: 0.0,
            options: .AllowUserInteraction,
            animations: { [weak self] _ in
                self?.tableFooterView? = UIView()
            },
            completion: nil
        )
    }
    
}

// MARK: - UICollectionView

extension UICollectionView {
    
    func registerCollectionViewCell(cell: ReusableCellIdentifiable.Type) {
        registerNib(
            UINib(
                nibName: String(cell),
                bundle: nil
            ),
            forCellWithReuseIdentifier: cell.reusableIdentifier
        )
    }
    
}

// MARK: - UIImageView

extension UIImageView {
    
    func setImageWithString(string: String) {
        guard let url = NSURL(string: string) else { return }
        
        sd_setImageWithURL(url,
            placeholderImage: UIImage(),
            options: SDWebImageOptions.AvoidAutoSetImage,
            progress: { _ in },
            completed: { [weak self] (image, error, cache, url) in
                self?.image = image
            }
        )
    }
    
}

// MARK: - Dictionary

extension Dictionary {
    
    // https://twitter.com/AirspeedSwift/status/618814897590247424/photo/1
    mutating func unionByOverwriting<S:SequenceType where S.Generator.Element == (Key, Value)>(sequence: S) {
        for (key, value) in sequence {
            self[key] = value
        }
    }
    
}

// MARK: - NSObject

extension NSObject {
    
    func topViewController() -> UIViewController? {
        guard let root = UIApplication.sharedApplication().delegate?.window??.rootViewController else {
            return nil
        }
        
        if root is UINavigationController {
            return (root as? UINavigationController)?.viewControllers.last
        }
        
        return root
    }
    
}