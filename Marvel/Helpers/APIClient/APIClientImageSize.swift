//
//  APIClientImageSize.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/26/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import Foundation

enum APIClientImageSize: String {
    case Full = ""
    case Portrait_XLarge = "/portrait_xlarge"
    case Standard_Medium = "/standard_medium"
    
    static func getFinalStringUrl(
        urlString: String,
        size: APIClientImageSize,
        extensionString: String) -> String
    {
        return "\(urlString)\(size.rawValue).\(extensionString)"
    }
    
}
