//
//  APIClient.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/26/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import Foundation

import Alamofire
import CryptoSwift
import ObjectMapper
import AlamofireObjectMapper


class APIClient {
    static let sharedInstance = APIClient()
    
    private let manager: Alamofire.Manager!
    
    private let baseUrl = "http://gateway.marvel.com/v1/public/"
    
    private let privateKey = "fe5f72357abe0aaccd5f9a7beedcaa9cfce19325"
    private let publicKey = "da84d26622df97f17e7bc6822077cc7e"
    
    private var timestamp: NSTimeInterval {
        return NSDate().timeIntervalSinceNow
    }
    
    private init() {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.timeoutIntervalForRequest = 15.0
        
        manager = Alamofire.Manager(configuration: configuration)
    }
    
    // MARK: Setup Methods
    
    private var defaultParameters: [String: String] {
        let ts = "\(timestamp)"
        
        return [
            "apikey": publicKey,
            "hash": "\(ts)\(privateKey)\(publicKey)".md5(),
            "ts": ts
        ]
    }
    
}

// MARK: - Generic Request Methods

extension APIClient {
    
    private func getObjectWithUrl<T: Mappable>(
        url: String,
        parameters: [String: String] = [String: String](),
        completion: (T? -> Void)) -> Request?
    {
        return manager.request(.GET, url,
            parameters: getFullParameters(extraParameters: parameters)).responseObject(
                completionHandler: { (response: Response<T, NSError>) in
                    if let code = response.result.error?.code where code == NSURLErrorCancelled { return }
                    completion(response.result.value)
                }
        )
    }
    
    private func getFullParameters(extraParameters parameters: [String: String]) -> [String: String] {
        var finalParameters = defaultParameters
        finalParameters.unionByOverwriting(parameters)
        
        return finalParameters
    }
    
}

// MARK: - Character requests

extension APIClient {
    
    func getCharacters(offset offset: Int = 0, nameStartsWith: String? = nil,limit: Int = 20, completion: (CharactersResponse? -> Void)) -> Request? {
        var parameters = [
            "orderBy": "name",
            "offset": "\(offset)",
            "limit": "\(limit)"
        ]
        
        if let name = nameStartsWith {
            parameters.unionByOverwriting(["nameStartsWith": name])
        }
        
        return getObjectWithUrl(
            "\(baseUrl)characters",
            parameters: parameters,
            completion: completion
        )
    }
    
    func getCardsWithCharacterId(characterId: Int, path: String, offset: Int = 0, limit: Int = 20, completion: (CardsResponse? -> Void)) -> Request? {
        return getObjectWithUrl(
            "\(baseUrl)characters/\(characterId)/\(path)",
            parameters: [
                "offset": "\(offset)",
                "limit": "\(limit)"
            ],
            completion: completion
        )
    }

}
