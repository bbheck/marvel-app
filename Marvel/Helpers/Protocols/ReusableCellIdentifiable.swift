//
//  ReusableCellIdentifiable.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/29/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

protocol ReusableCellIdentifiable {
    static var reusableIdentifier: String { get }
}

extension ReusableCellIdentifiable {
    static var reusableIdentifier: String {
        return String(self)
    }
}

extension UITableViewCell: ReusableCellIdentifiable {}
extension UICollectionViewCell: ReusableCellIdentifiable {}