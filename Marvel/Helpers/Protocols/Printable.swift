//
//  Printable.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/1/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import Foundation

protocol Printable {
    var name: String { get }
    var thumbnailPath: String { get }
    var thumbnailExtension: String { get }
    var fullImageUrlString: String { get }
}

extension Printable {
    
    var fullImageUrlString: String {
        return APIClientImageSize.getFinalStringUrl(
            thumbnailPath,
            size: .Full,
            extensionString: thumbnailExtension
        )
    }
    
}