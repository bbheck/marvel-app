//
//  CharacterDetailSection.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/1/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import Foundation

enum CharacterDetailSectionType {
    case Text
    case CardsList
    case Link
}

class CharacterDetailSection {
    let name: String
    let type: CharacterDetailSectionType
    let count: Int
    var content: [AnyObject]? = nil
    
    init(name: String, type: CharacterDetailSectionType, count: Int = 0) {
        self.name = name
        self.type = type
        self.count = count
    }
    
}