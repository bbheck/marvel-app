//
//  Card.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/30/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import ObjectMapper

class CardsResponse: Mappable {
    var etag: String = ""
    var data: CardsList?
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        etag    <- map["etag"]
        data    <- map["data"]
    }
    
}

class CardsList: Mappable {
    var offset: Int = 0
    var limit: Int = 0
    var total: Int = 0
    var count: Int = 0
    var results: [Card]?
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        offset  <- map["offset"]
        limit   <- map["limit"]
        total   <- map["total"]
        count   <- map["count"]
        results <- map["results"]
    }
    
}

class Card: Mappable, Printable {
    var id: Int = 0
    var name: String = ""
    var thumbnailPath: String = ""
    var thumbnailExtension: String = ""
    var portraintImageUrlString: String {
        return APIClientImageSize.getFinalStringUrl(
            thumbnailPath,
            size: .Portrait_XLarge,
            extensionString: thumbnailExtension
        )
    }
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        id                  <- map["id"]
        name                <- map["title"]
        thumbnailPath       <- map["thumbnail.path"]
        thumbnailExtension  <- map["thumbnail.extension"]
    }
    
}
