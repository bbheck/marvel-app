//
//  Character.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/26/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import ObjectMapper

class CharactersResponse: Mappable {
    var etag: String = ""
    var data: CharactersList?
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        etag    <- map["etag"]
        data    <- map["data"]
    }
    
}

class CharactersList: Mappable {
    var offset: Int = 0
    var limit: Int = 0
    var total: Int = 0
    var count: Int = 0
    var results: [Character]?
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        offset  <- map["offset"]
        limit   <- map["limit"]
        total   <- map["total"]
        count   <- map["count"]
        results <- map["results"]
    }
    
}

class CharacterUrl: Mappable {
    var type: String = ""
    var url: String = ""
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        type    <- map["type"]
        url     <- map["url"]
    }
}

class Character: Mappable, Printable {
    var id: Int = 0
    var name: String = ""
    var description: String = ""
    var thumbnailPath: String = ""
    var thumbnailExtension: String = ""
    var numComics: Int = 0
    var numSeries: Int = 0
    var numStories: Int = 0
    var numEvents: Int = 0
    var urls = [CharacterUrl]()
    
    var standardMediumUrlString: String {
        return APIClientImageSize.getFinalStringUrl(
            thumbnailPath,
            size: .Standard_Medium,
            extensionString: thumbnailExtension
        )
    }
    
    required init?(_ map: Map) {}
    
    func mapping(map: Map) {
        id                  <- map["id"]
        name                <- map["name"]
        description         <- map["description"]
        thumbnailPath       <- map["thumbnail.path"]
        thumbnailExtension  <- map["thumbnail.extension"]
        numComics           <- map["comics.available"]
        numSeries           <- map["series.available"]
        numStories          <- map["stories.available"]
        numEvents           <- map["events.available"]
        urls                <- map["urls"]
    }
    
}