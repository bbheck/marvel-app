//
//  CharacterDetailViewController.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/30/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class CharacterDetailViewController: BaseViewController {
    
    @IBOutlet private weak var topBarView: UIView!
    @IBOutlet private weak var tableView: UITableView!
    
    var headerView: CharacterDetailHeaderView? {
        return tableView.tableHeaderView as? CharacterDetailHeaderView
    }
    
    var headerViewHeight: CGFloat {
        return tableView.bounds.width * 0.5
    }
    
    var navigationBarHeight: CGFloat {
        return navigationController?.navigationBar.frame.height ?? 0
    }
    
    let scrollOffset: CGFloat = 20.0
    
    weak var character: Character?
    
    private var sections = [
        CharacterDetailSection(name: "NAME", type: .Text),
        CharacterDetailSection(name: "DESCRIPTION", type: .Text)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.clearColor()
        ]
        
        topBarView.addBlurWithStyle(.Dark)
        
        setupTableView()
        setupSections()
        
        loadContent()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        navigationController?.navigationBar.barTintColor = UIColor.clearColor()
        navigationController?.navigationBar.translucent = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        headerView?.frame.size.height = headerViewHeight
    }
    
    private func setupTableView() {
        tableView.registerTableViewCell(TextTableViewCell)
        tableView.registerTableViewCell(CardListTableViewCell)
        tableView.registerTableViewCell(LinkTableViewCell)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100.0
    }
    
    private func setupSections() {
        if character?.numComics > 0 {
            sections.append(
                CharacterDetailSection(name: "COMICS", type: .CardsList, count: character?.numComics ?? 0)
            )
        }
        
        if character?.numSeries > 0 {
            sections.append(
                CharacterDetailSection(name: "SERIES", type: .CardsList, count: character?.numSeries ?? 0)
            )
        }
        
        if character?.numStories > 0 {
            sections.append(
                CharacterDetailSection(name: "STORIES", type: .CardsList, count: character?.numStories ?? 0)
            )
        }
        
        if character?.numEvents > 0 {
            sections.append(
                CharacterDetailSection(name: "EVENTS", type: .CardsList, count: character?.numEvents ?? 0)
            )
        }
        
        if character?.urls.count > 0 {
            sections.append(CharacterDetailSection(name: "RELATED LINKS", type: .Link))
        }
    }
    
    private func loadContent() {
        guard let _character = character else {
            return
        }
        
        title = character?.name
        
        tableView.tableHeaderView = CharacterDetailHeaderView.createWithSize(
            CGSize(
                width: tableView.bounds.width,
                height: headerViewHeight
            ),
            stringUrl: _character.fullImageUrlString
        )
        
        setupBackgroundWithStringUrl(_character.fullImageUrlString)
        
        for (index, section) in sections.enumerate() {
            if section.type == .CardsList {
                getCardsWithPath(
                    section.name.lowercaseString,
                    sectionIndex: index,
                    limit: section.count
                )
            }
        }
    }
    
    private func setupBackgroundWithStringUrl(string: String) {
        let imageView = UIImageView(frame: tableView.bounds)
        imageView.setImageWithString(string)
        imageView.contentMode = .ScaleAspectFill
        imageView.addBlurWithStyle(.Dark)
        
        tableView.backgroundView = imageView
    }
    
    private func getCardsWithPath(path: String, sectionIndex: Int, limit: Int = 20) {
        APIClient.sharedInstance.getCardsWithCharacterId(
            character!.id,
            path: path,
            limit: limit,
            completion: { [weak self] (response) in
                self?.sections[sectionIndex].content = response?.data?.results
                self?.tableView.reloadRowsAtIndexPaths(
                    [
                        NSIndexPath(
                            forRow: 0,
                            inSection: sectionIndex
                        )
                    ],
                    withRowAnimation: .Automatic
                )
            }
        )
    }

}

// MARK: - UIScrollViewDelegate

extension CharacterDetailViewController {
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView == tableView {
            let startPoint = headerViewHeight - (navigationBarHeight + scrollOffset)
            let alpha = (tableView.contentOffset.y - startPoint) / scrollOffset
            
            headerView?.setContentOffset(scrollView.contentOffset)
            topBarView.alpha = alpha
            
            navigationController?.navigationBar.titleTextAttributes = [
                NSForegroundColorAttributeName: UIColor(
                    white: 1.0,
                    alpha: alpha
                )
            ]
        }
    }
    
}

// MARK: - UITableViewDataSource

extension CharacterDetailViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section].type {
        case .Link:
            return character?.urls.count ?? 0
        default:
            return 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch sections[indexPath.section].type {
        case .CardsList:
            let cell = tableView.dequeueReusableCellWithIdentifier(CardListTableViewCell.reusableIdentifier, forIndexPath: indexPath) as! CardListTableViewCell
            if let cards = sections[indexPath.section].content as? [Card] {
                cell.items = cards
            } else {
                cell.items = []
            }
            return cell
        case .Link:
            let cell = tableView.dequeueReusableCellWithIdentifier(LinkTableViewCell.reusableIdentifier, forIndexPath: indexPath) as! LinkTableViewCell
            cell.titleText = character?.urls[indexPath.row].type.capitalizedString
            return cell
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier(TextTableViewCell.reusableIdentifier, forIndexPath: indexPath) as! TextTableViewCell
            cell.titleText = indexPath.section == 0 ? character?.name : character?.description
            return cell
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].name
    }
    
}

// MARK: - UITableViewDelegate

extension CharacterDetailViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as? UITableViewHeaderFooterView
        
        headerView?.contentView.backgroundColor = UIColor.clearColor()
        headerView?.textLabel?.font = UIFont.boldSystemFontOfSize(14.0)
        headerView?.textLabel?.textColor = Config.redColor
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 26.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if sections[indexPath.section].type == .Link {
            LinkTableViewCell.openWithUrlString(character?.urls[indexPath.row].url)
        }
    }
    
}
