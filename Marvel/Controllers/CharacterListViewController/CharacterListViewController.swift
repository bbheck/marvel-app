//
//  HomeViewController.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/26/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

import Alamofire

class CharacterListViewController: BaseViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    lazy var searchViewController: SearchCharacterViewController = {
        return self.storyboard?.getViewController(SearchCharacterViewController)
    }()!
    
    lazy var errorAlertController: UIAlertController = {
        let alertController = UIAlertController(
            title: "Connection Error",
            message: "It was not possible to get the data from the server. Please check your connection and try again.",
            preferredStyle: .Alert
        )
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        alertController.addAction(
            UIAlertAction(
                title: "Try Again",
                style: .Default,
                handler: { [weak self] _ in
                    self?.getCharacters()
                }
            )
        )
        
        return alertController
    }()
    
    weak var loadMoreRequest: Request?
    
    private var characters = [Character]()
    
    private var loadMore = true {
        didSet {
            if !loadMore {
                tableView.removeFooterViewWithAnimation()
            }
        }
    }
    var selectedCharacter: Character?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.titleView = UIImageView(
            image: UIImage(
                named: "Logo"
            )
        )
        
        setupTableView()
        
        getCharacters()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.barTintColor = Config.navigationBarColor
        navigationController?.navigationBar.translucent = false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segue.toCharacterDetail {
            (segue.destinationViewController as? CharacterDetailViewController)?.character = selectedCharacter
        }
    }
    
    private func setupTableView() {
        tableView.registerTableViewCell(CharacterListTableViewCell)
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        activityIndicator.startAnimating()
        activityIndicator.frame = CGRect(
            x: 0.0, y: 0.0,
            width: tableView.bounds.width,
            height: 50.0
        )
        tableView.tableFooterView = activityIndicator
    }
    
    private func getCharacters() {
        loadMoreRequest?.cancel()
        loadMoreRequest = APIClient.sharedInstance.getCharacters(
            offset: characters.count,
            completion: { [weak self] (response) in
                guard let results = response?.data?.results else {
                    self?.showErrorAlert()
                    return
                }
                
                let initialCount = self?.characters.count ?? 0
                
                self?.characters += results
                self?.updateTableViewWithInitialCount(initialCount)
                
                if self?.characters.count == response?.data?.total {
                    self?.loadMore = false
                }
            }
        )
    }
    
    private func updateTableViewWithInitialCount(initialCount: Int) {
        tableView.insertRowsAtIndexPaths(
            (initialCount..<characters.count).flatMap({ NSIndexPath(forRow: $0, inSection: 0) }),
            withRowAnimation: .Automatic
        )
    }
    
    private func showErrorAlert() {
        presentViewController(errorAlertController, animated: true, completion: nil)
    }

}

// MARK: - Actions

extension CharacterListViewController {
    
    @IBAction func searchAction() {
        // FIXME: Bad implementation.
        // Check another examples to add/create a UISearchController
        addChildViewController(searchViewController)
        view.addSubview(searchViewController.view)
        searchViewController.didMoveToParentViewController(self)
        
        searchViewController.view.frame.size.height = view.bounds.height
        searchViewController.view.frame.origin.y = view.bounds.height
        
        UIView.transitionWithView(
            view,
            duration: 0.4,
            options: .TransitionNone,
            animations: { [weak self] _ in
                self?.searchViewController.view.frame.origin.y = 0
            },
            completion: { [weak self] _ in
                self?.searchViewController.searchController.searchBar.becomeFirstResponder()
            }
        )
    }
    
}

// MARK: - UITableViewDataSource

extension CharacterListViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CharacterListTableViewCell.reusableIdentifier) as! CharacterListTableViewCell
        
        cell.character = characters[indexPath.row]
        
        return cell
    }
    
}

// MARK: - UITableViewDataSource

extension CharacterListViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        (cell as? CharacterListTableViewCell)?.loadContent()
        
        if loadMore && indexPath.row + 1 == characters.count {
            getCharacters()
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.frame.width * CharacterListTableViewCell.ratio
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedCharacter = characters[indexPath.row]
        performSegueWithIdentifier(Segue.toCharacterDetail, sender: self)
    }
    
}