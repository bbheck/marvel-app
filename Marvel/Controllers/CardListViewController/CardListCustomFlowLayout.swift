
//
//  CardList.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/2/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class CardListCustomFlowLayout: UICollectionViewFlowLayout {
    
    override func awakeFromNib() {
        scrollDirection = .Horizontal
    }
    
    // https://gist.github.com/mmick66/9812223#gistcomment-1436413
    override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        let rectBounds = collectionView!.bounds
        let halfWidth = rectBounds.size.width * 0.5
        let proposedContentOffsetCenterX = proposedContentOffset.x + halfWidth
        
        let proposedRect = collectionView!.bounds
        let attributesArray = layoutAttributesForElementsInRect(proposedRect)!
        
        var candidateAttributes: UICollectionViewLayoutAttributes?
        
        for layoutAttributes in attributesArray {
            
            if layoutAttributes.representedElementCategory != UICollectionElementCategory.Cell {
                continue
            }
            
            if candidateAttributes == nil {
                candidateAttributes = layoutAttributes
                continue
            }
            
            if fabsf(Float(layoutAttributes.center.x) - Float(proposedContentOffsetCenterX)) < fabsf(Float(candidateAttributes!.center.x) - Float(proposedContentOffsetCenterX)) {
                candidateAttributes = layoutAttributes
            }
            
        }
        
        if attributesArray.count == 0 {
            return CGPoint(x: proposedContentOffset.x - halfWidth * 2,y: proposedContentOffset.y)
        }
        
        return CGPoint(x: candidateAttributes!.center.x - halfWidth, y: proposedContentOffset.y)
    }
    
}