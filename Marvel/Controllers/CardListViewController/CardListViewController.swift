//
//  CardListViewController.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/1/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class CardListViewController: BaseViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    let offset: CGFloat = 30.0
    
    var items = [Card]()
    var selectedRow: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.registerCollectionViewCell(CardDetailCollectionViewCell)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        collectionView.scrollToItemAtIndexPath(
            NSIndexPath(
                forRow: selectedRow,
                inSection: 0
            ),
            atScrollPosition: .CenteredHorizontally,
            animated: true
        )
    }

}

// MARK: - Actions

extension CardListViewController {
    
    @IBAction func closeAction() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}

// MARK: - UICollectionViewDataSource

extension CardListViewController: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CardDetailCollectionViewCell.reusableIdentifier, forIndexPath: indexPath) as! CardDetailCollectionViewCell
        cell.card = items[indexPath.row]
        
        return cell
    }
    
}

// MARK: - UICollectionViewDelegate

extension CardListViewController: UICollectionViewDelegate {

    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        (cell as? CardDetailCollectionViewCell)?.loadContent(index: indexPath.row, total: items.count)
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension CardListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(
            width: collectionView.bounds.width - (2 * offset),
            height: collectionView.bounds.height - offset
        )
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return offset / 2
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(
            top: 0.0,
            left: offset,
            bottom: 0.0,
            right: offset
        )
    }
    
}