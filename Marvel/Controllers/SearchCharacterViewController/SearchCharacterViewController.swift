//
//  SearchCharacterViewController.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 5/1/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

import Alamofire

class SearchCharacterViewController: BaseViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    weak var loadMoreRequest: Request?
    
    private var characters = [Character]() {
        didSet {
            if characters.count == 0 {
                loadMore = true
                tableView.reloadData()
            }
        }
    }
    
    private var loadMore = true {
        didSet {
            if !loadMore {
                tableView.removeFooterViewWithAnimation()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSearchController()
        setupTableView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        setStatusBarBackgroundColor(Config.navigationBarColor)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        setStatusBarBackgroundColor(UIColor.clearColor())
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    private func setupSearchController() {
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.barTintColor = Config.navigationBarColor
        searchController.searchBar.tintColor = Config.redColor
        
        definesPresentationContext = true
    }
    
    private func setupTableView() {
        tableView.registerTableViewCell(SearchItemTableViewCell)
        
        tableView.scrollsToTop = true
        tableView.tableHeaderView = searchController.searchBar
        
        addTableFooterView()
    }
    
    private func addTableFooterView() {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        activityIndicator.hidden = true
        activityIndicator.frame = CGRect(
            x: 0.0, y: 0.0,
            width: tableView.bounds.width,
            height: 50.0
        )
        tableView.tableFooterView = activityIndicator
    }
    
    private func getCharactersWithName(name: String) {
        (tableView.tableFooterView as? UIActivityIndicatorView)?.startAnimating()
        
        loadMoreRequest?.cancel()
        loadMoreRequest = APIClient.sharedInstance.getCharacters(
            nameStartsWith: name,
            offset: characters.count,
            completion: { [weak self] (response) in
                (self?.tableView.tableFooterView as? UIActivityIndicatorView)?.stopAnimating()
                guard let results = response?.data?.results else {
                    return
                }
                
                let initialCount = self?.characters.count ?? 0
                
                self?.characters += results
                self?.updateTableViewWithInitialCount(initialCount)
                
                if self?.characters.count == response?.data?.total {
                    self?.loadMore = false
                }
            }
        )
    }
    
    private func updateTableViewWithInitialCount(initialCount: Int) {
        tableView.insertRowsAtIndexPaths(
            (initialCount..<characters.count).flatMap({ NSIndexPath(forRow: $0, inSection: 0) }),
            withRowAnimation: .Automatic
        )
    }

}

extension SearchCharacterViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(SearchItemTableViewCell.reusableIdentifier, forIndexPath: indexPath) as! SearchItemTableViewCell
        
        cell.character = characters[indexPath.row]
        
        return cell
    }
    
}

extension SearchCharacterViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        (cell as? SearchItemTableViewCell)?.loadContent()
        
        if loadMore && indexPath.row + 1 == characters.count {
            getCharactersWithName(searchController.searchBar.text ?? "")
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let topViewContorller = topViewController() as? CharacterListViewController else {
            return
        }
        
        topViewContorller.selectedCharacter = characters[indexPath.row]
        topViewContorller.performSegueWithIdentifier(Segue.toCharacterDetail, sender: self)
    }
    
}

extension SearchCharacterViewController: UISearchBarDelegate {
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        characters.removeAll(keepCapacity: true)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        characters.removeAll(keepCapacity: true)
        addTableFooterView()
        getCharactersWithName(searchBar.text ?? "")
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        UIView.transitionWithView(
            view,
            duration: 0.4,
            options: .TransitionNone,
            animations: { [weak self] _ in
                guard let `self` = self else { return }
                self.view.frame.origin.y = self.view.bounds.height
            },
            completion: { [weak self] _ in
                self?.characters.removeAll()
                self?.view.removeFromSuperview()
                self?.removeFromParentViewController()
            }
        )
    }
    
}
