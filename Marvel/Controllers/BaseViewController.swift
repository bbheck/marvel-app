//
//  BaseViewController.swift
//  Marvel
//
//  Created by Bruno Hecktheuer on 4/30/16.
//  Copyright © 2016 Bruno Hecktheuer. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Config.backgroundColor
        
        navigationItem.backBarButtonItem = UIBarButtonItem(
            title: " ", style: .Plain,
            target: nil, action: nil
        )
    }

}
