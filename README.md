# Marvel App

## How to run

Open `Marvel.xcworkspace` and run the project.

## External libraries

- `Alamofire`
- `AlamofireObjectMapper`
- `SDWebImage`
- `CryptoSwift` 
